package models;

public class Rectangle extends Shape {
    private Integer length;
    private Integer width;

    
    public Rectangle(String color, Integer length, Integer width) {
        super(color);
        this.length = length;
        this.width = width;
    }

    @Override
    public double getArea() {
        // TODO Auto-generated method stub
        return this.length * this.width;
    }

    @Override
    public String toString() {
        return "Rectangle [length=" + length + ", width=" + width + ", getColor()=" + getColor() + "]";
    }
}
