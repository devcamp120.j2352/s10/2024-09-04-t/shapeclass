package models;

public class Triangle extends Shape {
    private Integer base;
    private Integer height;
    
    public Triangle(String color, Integer base, Integer height) {
        super(color);
        this.base = base;
        this.height = height;
    }

    @Override
    public String toString() {
        return "Triangle [base=" + base + ", height=" + height + ", getColor()=" + getColor() + "]";
    }

    @Override
    public double getArea() {        
        return (this.base*this.height)/2;
    }

    public Integer getBase() {
        return base;
    }

    public void setBase(Integer base) {
        this.base = base;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

}
