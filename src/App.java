import models.Rectangle;
import models.Triangle;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Rectangle r1 = new Rectangle("Red", 10, 5);
        Rectangle r2 = new Rectangle("Yellow", 20, 8);

        System.out.println(r1.toString());
        System.out.println("Area: "+ r1.getArea());

        System.out.println(r2.toString());
        System.out.println("Area: "+ r2.getArea());

        Triangle t1 = new Triangle("White", 10, 20);
        Triangle t2 = new Triangle("Blue", 15, 30);

        System.out.println(t1.toString());
        System.out.println("Area: "+ t1.getArea());

        System.out.println(t2.toString());
        System.out.println("Area: "+ t2.getArea());
    }
}
